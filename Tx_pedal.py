def on_on_event():
    global dire
    if pins.digital_read_pin(DigitalPin.P2) == 0:
        if dire == 0:
            dire = 1
        else:
            dire = 0
        basic.pause(600)
    basic.pause(50)
    control.raise_event(4800, 4803)
control.on_event(4800, 4803, on_on_event)

def on_on_event2():
    radio.send_value("left", pins.analog_read_pin(AnalogPin.P0))
    radio.send_value("right", pins.analog_read_pin(AnalogPin.P1))
    radio.send_value("dire", dire)
control.on_event(4900, 4902, on_on_event2)

dire = 0
pins.set_pull(DigitalPin.P2, PinPullMode.PULL_UP)
basic.show_string("pe tx2")
basic.show_icon(IconNames.SQUARE)
basic.pause(200)
dire = 1
radio.set_group(180)
control.raise_event(4800, 4803)

def on_every_interval():
    if dire == 1:
        basic.show_leds("""
            . . # . .
                        . # # # .
                        # . # . #
                        . . # . .
                        . . # . .
        """)
    else:
        basic.show_leds("""
            . . # . .
                        . . # . .
                        # . # . #
                        . # # # .
                        . . # . .
        """)
loops.every_interval(200, on_every_interval)

def on_every_interval2():
    led.set_brightness(Math.constrain(pins.map(input.light_level(), 0, 200, 20, 255), 20, 255))
loops.every_interval(5000, on_every_interval2)
