def on_on_event():
    global dataRdy
    dataRdy = "NA"
    radio.raise_event(4900, 4901)
    basic.pause(30)
    if dataRdy == "MO":
        motion_drive()
    dataRdy = "NA"
    radio.raise_event(4900, 4902)
    basic.pause(30)
    if dataRdy == "PE":
        pedal_drive()
    control.raise_event(4800, 4801)
control.on_event(4800, 4801, on_on_event)

def pedal_drive_DR():
    global pwm1, pwm2
    pwm1 = Math.constrain(pins.map(left, 420, 1000, 100, 1023), 100, 1023)
    pwm2 = Math.constrain(pins.map(right, 420, 1000, 100, 1023), 100, 1023)
    if dire == 1:
        drive_motor("D", pwm1, "D", pwm2)
    else:
        drive_motor("R", pwm1, "R", pwm2)
def motion_drive_DR():
    global pwm1, pwmRatio, pwm2
    pwm1 = Math.constrain(pins.map(absaccz, 160, 600, 260, pwmMax), 260, pwmMax)
    pwmRatio = Math.constrain(pins.map(absaccx, 600, 160, ratioLRmin, 1), ratioLRmin, 1)
    pwm2 = pwm1 * pwmRatio
    if accz > 0:
        if accx < 0:
            drive_motor("R", pwm2, "R", pwm1)
        else:
            drive_motor("R", pwm1, "R", pwm2)
    elif accx < 0:
        drive_motor("D", pwm2, "D", pwm1)
    else:
        drive_motor("D", pwm1, "D", pwm2)
def pedal_drive():
    global driveBy, subLR, abssubLR, motorStopTmo
    driveBy = "PE"
    subLR = left - right
    abssubLR = abs(subLR)
    if dire == 0 and abssubLR > 160:
        pedal_drive_U()
    else:
        pedal_drive_DR()
    motorStopTmo = 10
def motion_drive_U():
    global pwm1
    pwm1 = Math.constrain(pins.map(absaccx, 160, 600, 300, pwmMax), 300, pwmMax)
    if accx < 0:
        drive_motor("R", pwm1, "D", pwm1)
    else:
        drive_motor("D", pwm1, "R", pwm1)
def stop_motor():
    global accx, accz, left, right, dataRdy, driveBy, motorStopTmo
    pins.digital_write_pin(DigitalPin.P14, 0)
    pins.analog_write_pin(AnalogPin.P1, 0)
    pins.analog_write_pin(AnalogPin.P2, 0)
    accx = 0
    accz = 0
    left = 0
    right = 0
    dataRdy = "NA"
    driveBy = "NA"
    motorStopTmo = 0

def on_received_value(name, value):
    global mode, accz, accx, absaccx, absaccz, dataRdy, left, right, dire
    if name == "mode":
        mode = value
    elif name == "accz":
        accz = value
    elif name == "accx":
        accx = value
        absaccx = abs(accx)
        absaccz = abs(accz)
        if absaccx >= 200 or absaccz >= 200:
            dataRdy = "MO"
    elif name == "left":
        left = value
    elif name == "right":
        right = value
    elif name == "dire":
        dire = value
        if left >= 420 or right >= 420:
            dataRdy = "PE"
radio.on_received_value(on_received_value)

def motion_drive():
    global driveBy, motorStopTmo
    driveBy = "MO"
    if absaccz >= 160 or absaccx >= 160:
        if absaccz < 160:
            motion_drive_U()
        else:
            motion_drive_DR()
        motorStopTmo = 10
def pedal_drive_U():
    global pwm1
    pwm1 = Math.constrain(pins.map(abssubLR, 160, 320, 100, 1023), 100, 1023)
    if left > right:
        drive_motor("D", pwm1, "R", pwm1)
    else:
        drive_motor("R", pwm1, "D", pwm1)
def drive_motor(L_DR: str, L_PWM: number, R_DR: str, R_PWM: number):
    if L_DR == "D":
        pins.digital_write_pin(DigitalPin.P13, 1)
    elif L_DR == "R":
        pins.digital_write_pin(DigitalPin.P13, 0)
    if R_DR == "D":
        pins.digital_write_pin(DigitalPin.P8, 1)
    elif R_DR == "R":
        pins.digital_write_pin(DigitalPin.P8, 0)
    pins.analog_write_pin(AnalogPin.P2, L_PWM)
    pins.analog_write_pin(AnalogPin.P1, R_PWM)
    pins.digital_write_pin(DigitalPin.P14, 1)
motorStopTmo = 0
abssubLR = 0
subLR = 0
driveBy = ""
accx = 0
accz = 0
absaccx = 0
pwmRatio = 0
absaccz = 0
dire = 0
right = 0
pwm2 = 0
left = 0
pwm1 = 0
dataRdy = ""
ratioLRmin = 0
pwmMax = 0
mode = 0
stop_motor()
basic.show_string("rx2")
basic.pause(500)
basic.show_icon(IconNames.SQUARE)
mode = 0
pwmMax = 600
ratioLRmin = 0.3
radio.set_group(180)
control.raise_event(4800, 4801)

def on_every_interval():
    global motorStopTmo
    if motorStopTmo > 0:
        motorStopTmo += -1
        if motorStopTmo == 0:
            stop_motor()
loops.every_interval(50, on_every_interval)

def on_every_interval2():
    led.stop_animation()
    if driveBy == "MO":
        led.plot(Math.constrain(pins.map(accx, -600, 600, 0, 5), 0, 4),
            Math.constrain(pins.map(accz, -600, 600, 0, 5), 0, 4))
    elif driveBy == "PE":
        if dire == 1:
            basic.show_leds("""
                . . # . .
                                . # # # .
                                # . # . #
                                . . # . .
                                . . # . .
            """)
        else:
            basic.show_leds("""
                . . # . .
                                . . # . .
                                # . # . #
                                . # # # .
                                . . # . .
            """)
    else:
        basic.show_leds("""
            . . . . .
                        . . . . .
                        # # # # #
                        . . . . .
                        . . . . .
        """)
loops.every_interval(250, on_every_interval2)

def on_every_interval3():
    global pwmMax, ratioLRmin
    if mode == 0:
        pwmMax = 600
        ratioLRmin = 0.3
    else:
        pwmMax = 1023
        ratioLRmin = 0.6
loops.every_interval(3000, on_every_interval3)

def on_every_interval4():
    led.set_brightness(Math.constrain(pins.map(input.light_level(), 0, 200, 20, 255), 20, 255))
loops.every_interval(5000, on_every_interval4)
